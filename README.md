# Demo Project

#### 说明

> 除了以下成熟分支外，本项目还存在笔者平时进行测试的相关实验分支，这些分支无需关注

| 序号  | 标签                                                                                       | demo说明                                                | demo所处分支                                                    |
|-----|------------------------------------------------------------------------------------------|-------------------------------------------------------|-------------------------------------------------------------|
| 1   | <font  face="幼圆" color = "#F53B45">**knife4j、api文档**</font>                              | spring-boot2集成knife4j-swagger2                        | knife4j-swagger2-springboot2                                |
|     |                                                                                          | spring-boot2集成knife4j-swagger3                        | knife4j-swagger3-springboot2                                |
| 2   | <font  face="幼圆" color = "#F53B45">**k8s、kubesphere、kubesphere dev-ops、应用部署至k8s**</font> | kubesphere通过devOps部署应用至k8s                            | kubesphere-devOp                                            |
| 3   | <font  face="幼圆" color = "#F53B45">**saml2、saml idp、 saml2基于角色方式sso至阿里云**</font>         | 基于saml2，我方作为idp， 基于角色的方式sso至阿里云                       | role-based_smal_sso_2_aliyun                                |
| 4   | <font  face="幼圆" color = "#F53B45">**建木流水线、nodejs、前端部署**</font>                          | 建木nodejs构建前端代码并制作镜像推送至镜像仓库示例（没有最后一步拉取启动镜像，较简单，自行补充即可） | frontend/vue-demo                                           |
| 5   | <font  face="幼圆" color = "#F53B45">**经纬度数据处理、geo数据、redis处理geo数据**</font>                 | 利用redis处理geo地理位置数据demo                                | redis/geo-demo                                              |
| 6   | <font  face="幼圆" color = "#F53B45">**spring-boot、rocketmq**</font>                       | spring-boot2集成rocketmq<br />环境：RocketMQ5.1.0          | rocketmq/demo                                               |
| 7   | <font  face="幼圆" color = "#F53B45">**spring-cloud-stream、rocketmq**</font>               | spring-cloud-stream整合rocketmq<br />环境：RocketMQ5.1.0   | rocketmq/spring-cloud-stream/demo                           |
| 8   | <font  face="幼圆" color = "#F53B45">**mybatis-plus测试**</font>                             | mybatis-plus测试                                        | mybatis-plus/test                                           |
| 9   | <font  face="幼圆" color = "#F53B45">**spring-brick、spring-boot插件化**</font>                | spring-boot集成spring-brick实现动态插件                       | spring-brick/demo                                           |
| 10  | <font  face="幼圆" color = "#F53B45">**spring-boot优雅停机、内嵌tomcat优雅停机**</font>               | spring-boot优雅停机                                       | spring-boot/shutdown-graceful                               |
| 11  | <font  face="幼圆" color = "#F53B45">**seata、分布式事务、AT模式测试**</font>                         | spring-cloud seata分布式事务AT模式demo                       | spring-cloud/seata/demo                                     |
| 12  | <font  face="幼圆" color = "#F53B45">**shardingsphere + seata AT、分布式事务、分库分表**</font>       | shardingsphere分布式事务BASE模式（seata AT实现方式）整合             | spring-cloud/seata/shardingsphere-transaction-base-seata-at |
| 13  | <font  face="幼圆" color = "#F53B45">**spring-boot集成nacos、spring-boot整合nacos**</font>      | spring-boot集成nacos                                    | nacos/spring-boot-nacos                                     |
| 15  | <font  face="幼圆" color = "#F53B45">**mybatis-plus扩展、硬删除、硬查询**</font>                     | 扩展mybatis-plus，保留逻辑删除、逻辑查询的前提下，支持硬删除、硬查询              | mybatis-plus/force-select-delete                            |
| 16  | <font  face="幼圆" color = "#F53B45">**scheduled、定时任务**</font>                             | 定时任务scheduled测试                                       | scheduled                                                   |
| 17  | <font  face="幼圆" color = "#F53B45">**文档解析、tika**</font>                                  | 文档解析                                                  | apache-tika                                                 |
| 18  | <font  face="幼圆" color = "#F53B45">**执行本地shell（实现备份等）**</font>                           | 执行本地脚本以实现备份等功能                                        | backup/local-shell-executor                                 |
| 19  | <font  face="幼圆" color = "#F53B45">**excel实现下拉框、easyexcel**</font>                       | excel实现下拉框                                            | easyexcel/drop-down                                         |
| 20  | <font  face="幼圆" color = "#F53B45">**excel动态表头、easyexcel**</font>                        | excel实现动态表头                                           | easyexcel/dynamic-header-export                             |
| 21  | <font  face="幼圆" color = "#F53B45">**excel文件加密解密、easyexcel**</font>                      | excel文件加密解密                                           | easyexcel/excel-encrypt-decrypt                             |
| 22  | <font  face="幼圆" color = "#F53B45">**读取excel文件中的图片、easyexcel**</font>                    | 读取excel文件中的图片                                         | easyexcel/read-picture                                      |
| 23  | <font  face="幼圆" color = "#F53B45">**excel导出图片支持、easyexcel**</font>                      | excel导出图片支持                                           | easyexcel/url-export-picture                                |
| 24  | <font  face="幼圆" color = "#F53B45">**word导出支持复选框、easyword**</font>                       | word导出支持复选框                                           | easyword/checkbox                                           |
| 25  | <font  face="幼圆" color = "#F53B45">**word导出**</font>                                     | word导出                                                | easyword/demo                                               |
| 26  | <font  face="幼圆" color = "#F53B45">**word转pdf**</font>                                   | word转pdf                                              | word2pdf/aspose                                             |
| 27  | <font  face="幼圆" color = "#F53B45">**sentinel、服务降级、SentinelResource**</font>             | spring-boot集成sentinel注解                               | sentinel/SentinelResource                                   |
| 28  | <font  face="幼圆" color = "#F53B45">**jsr303、参数校验、validation**</font>                     | spring-boot使用validation                               | validation/demo                                             |
| 29  | <font  face="幼圆" color = "#F53B45">**耗时统计、TimeWatcher**</font>                           | 耗时统计器                                                 | timewatcher/demo                                            |
| 30  | <font  face="幼圆" color = "#F53B45">**动态线程池**</font>                           | 动态线程池                                                 | dynamic-tp/demo                                            |
