package com.ideaaedi.demo;

import com.ideaaedi.commonspring.annotation.EnableFeature;
import com.ideaaedi.commonspring.annotation.ParameterRecorder;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * start-class
 */
@SpringBootApplication
@EnableFeature(enableParameterRecorder = @ParameterRecorder(includePrefixes = "com.ideaaedi.demo"))
public class StartUp implements ApplicationRunner {
    public static void main(String[] args) {
        SpringApplication.run(StartUp.class, args);
    }
    
    @Override
    public void run(ApplicationArguments args) throws Exception {
    }
}
